package codepath.demos.helloworlddemo.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;

import java.util.List;

import codepath.demos.helloworlddemo.R;

public class PopUpHelper {

    public static void popupRadio(Context ctx, String ttl, List<AppCompatRadioButton> radio) {
        float scale = ctx.getResources().getDisplayMetrics().density;
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_daftar_kawasan, null);
        AppCompatTextView title = v.findViewById(R.id.title);
        AppCompatImageView close = v.findViewById(R.id.close);
        RadioGroup listItem = v.findViewById(R.id.list_item);
        title.setText(ttl);

        AlertDialog ad = new AlertDialog.Builder(ctx).create();
        ad.setButton(DialogInterface.BUTTON_POSITIVE, ctx.getString(android.R.string.ok), (d, i) -> d.dismiss());
        ad.setView(v);
        ad.show();

        int dpAsPixels = (int) (10 * scale + 0.5f);
        for (AppCompatRadioButton r : radio) {
            r.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT));
            r.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
            listItem.addView(r);
        }
        close.setOnClickListener(view -> ad.dismiss());
    }
}
