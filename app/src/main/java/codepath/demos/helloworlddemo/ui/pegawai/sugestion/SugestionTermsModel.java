package codepath.demos.helloworlddemo.ui.pegawai.sugestion;

public class SugestionTermsModel {

    private String id;
    private String title;
    private String uverif;
    private String utelaah;
    private String kverif;
    private String ktelaah;
    private String template;
    private String berkas;
    private String format;

    public SugestionTermsModel() {
    }

    public SugestionTermsModel(String id, String title, String uverif, String utelaah, String kverif, String ktelaah, String template, String berkas, String format) {
        this.id = id;
        this.title = title;
        this.uverif = uverif;
        this.utelaah = utelaah;
        this.kverif = kverif;
        this.ktelaah = ktelaah;
        this.template = template;
        this.berkas = berkas;
        this.format = format;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUverif() {
        return uverif;
    }

    public void setUverif(String uverif) {
        this.uverif = uverif;
    }

    public String getUtelaah() {
        return utelaah;
    }

    public void setUtelaah(String utelaah) {
        this.utelaah = utelaah;
    }

    public String getKverif() {
        return kverif;
    }

    public void setKverif(String kverif) {
        this.kverif = kverif;
    }

    public String getKtelaah() {
        return ktelaah;
    }

    public void setKtelaah(String ktelaah) {
        this.ktelaah = ktelaah;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getBerkas() {
        return berkas;
    }

    public void setBerkas(String berkas) {
        this.berkas = berkas;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}
