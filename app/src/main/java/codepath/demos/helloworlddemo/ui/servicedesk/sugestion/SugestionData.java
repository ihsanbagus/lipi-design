package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

import java.util.ArrayList;
import java.util.List;

public class SugestionData {

    public List<SugestionModel> getListData() {
        List<SugestionModel> ls = new ArrayList<>();
        SugestionModel sm;
        sm = new SugestionModel(
                "1",
                "Laporan lahiran anak ke 5",
                "21 Juni 2020 - 13:00"
        );
        ls.add(sm);
        sm = new SugestionModel(
                "2",
                "Laporan lahiran anak ke 5",
                "21 Juni 2020 - 13:00"
        );
        ls.add(sm);
        sm = new SugestionModel(
                "3",
                "Laporan lahiran anak ke 5",
                "21 Juni 2020 - 13:00"
        );
        ls.add(sm);
        sm = new SugestionModel(
                "4",
                "Laporan lahiran anak ke 5",
                "21 Juni 2020 - 13:00"
        );
        ls.add(sm);
        return ls;
    }

    public SugestionModel getDetailData(int id) {
        return getListData().get(id);
    }

    public List<SugestionTermsModel> getListTermsData() {
        List<SugestionTermsModel> ls = new ArrayList<>();
        SugestionTermsModel sm;
        sm = new SugestionTermsModel(
                "1",
                "Formulir penambahan anak",
                "-",
                "-",
                "-",
                "-",
                "template.docx",
                "berkas.docx",
                "format.docx"
        );
        ls.add(sm);
        sm = new SugestionTermsModel(
                "2",
                "Salinan akte kelahiran anak",
                "-",
                "-",
                "-",
                "-",
                "template.docx",
                "berkas.docx",
                "format.docx"
        );
        ls.add(sm);
        return ls;
    }

    public List<SugestionProofModel> getListProofData() {
        List<SugestionProofModel> ls = new ArrayList<>();
        SugestionProofModel sm;
        sm = new SugestionProofModel(
                "1",
                "Draft Penerbitan SK Aktif Kembali Tugas Belajar Jabatan Fungsional",
                "file_panduan.pdf",
                "-",
                "-",
                "-",
                "-");
        ls.add(sm);
        return ls;
    }

}
