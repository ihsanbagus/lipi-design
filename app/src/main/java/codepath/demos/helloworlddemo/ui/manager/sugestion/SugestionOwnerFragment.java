package codepath.demos.helloworlddemo.ui.manager.sugestion;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import codepath.demos.helloworlddemo.R;

public class SugestionOwnerFragment extends Fragment {

    private Context ctx;

    public SugestionOwnerFragment() {
    }

    public static SugestionOwnerFragment newInstance(Context ctx) {
        SugestionOwnerFragment f = new SugestionOwnerFragment();
        f.ctx = ctx;
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_servicedesk_sugestion_owner, container, false);
        return rootView;
    }
}
