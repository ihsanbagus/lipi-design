package codepath.demos.helloworlddemo.ui.manager.sugestion;

import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import codepath.demos.helloworlddemo.R;

public class PDFViewerActivity extends AppCompatActivity {
    private SwipeRefreshLayout refresh;
    private ProgressBar loading;
    private String url = "";
    private WebView webview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viewer);
        setToolbar();
        url = getIntent().getStringExtra("url");

        refresh = findViewById(R.id.refresh);
        loading = findViewById(R.id.loading);

        webview = findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setAppCacheEnabled(false);
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setDefaultTextEncodingName("utf-8");
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDisplayZoomControls(false);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String uri) {
                super.onPageFinished(view, uri);
                loading.setVisibility(View.INVISIBLE);
                url = uri;
                refresh.setRefreshing(false);
            }
        });
        //https://docs.google.com/viewer?url=http://yourdomain.com/yourpdf.pdf&hl=nl&embedded=true
        webview.loadUrl("https://drive.google.com/viewer?embedded=true&url=" + url);
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                loading.setProgress(progress + 10);
            }
        });

        refresh.setOnRefreshListener(() -> webview.loadUrl("https://drive.google.com/viewer?embedded=true&url=" + url));
    }

    private void setToolbar() {
        String name = getIntent().getStringExtra("name");
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(name);
        toolbar.setBackgroundColor(getResources().getColor(R.color.blue_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.blue_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}