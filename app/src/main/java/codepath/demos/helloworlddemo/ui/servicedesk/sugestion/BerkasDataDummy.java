package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

import java.util.ArrayList;
import java.util.List;

public class BerkasDataDummy {

    public List<BerkasModel> getListBerkas() {
        List<BerkasModel> bm = new ArrayList<>();
        for (int x = 0; x < 20; x++) {
            bm.add(new BerkasModel("Ijazah " + x, "Keterangan " + x, "http://www.adobe.com/devnet/acrobat/pdfs/pdf_open_parameters.pdf"));
        }
        return bm;
    }
}
