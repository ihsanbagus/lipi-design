package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import codepath.demos.helloworlddemo.R;

public class SugestionTermsUploadActivity extends AppCompatActivity {

    private AppCompatImageButton change;
    private AppCompatButton file;
    private AppCompatButton url;
    private AppCompatTextView file_name;

    private int PICKFILE_REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugestion_terms_upload);
        setToolbar();
        init();
    }

    private void init() {
        change = findViewById(R.id.change);
        file = findViewById(R.id.file);
        url = findViewById(R.id.url);
        file_name = findViewById(R.id.file_name);

        change.setOnClickListener(v -> {
            if (file.getVisibility() == View.VISIBLE) {
                file.setVisibility(View.GONE);
                url.setVisibility(View.VISIBLE);
            } else {
                url.setVisibility(View.GONE);
                file.setVisibility(View.VISIBLE);
            }
        });

        file.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("file/*");
            startActivityForResult(intent, PICKFILE_REQUEST_CODE);
        });

        url.setOnClickListener(v -> {
            List<BerkasModel> bm = new BerkasDataDummy().getListBerkas();
            popupFile(this, "Pilih Berkas", bm);
        });
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.terms_upload);
        toolbar.setBackgroundColor(getResources().getColor(R.color.green_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.green_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == PICKFILE_REQUEST_CODE) {
                String path = data.getData().getPath();
                String[] fn = path.split("/");
                file_name.setText(fn[fn.length - 1]);
            }
        } catch (Exception ignored) {
        }
    }

    public void popupFile(Context ctx, String ttl, List<BerkasModel> berkas) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.dialog_daftar_berkas, null);
        AppCompatTextView title = v.findViewById(R.id.title);
        AppCompatImageView close = v.findViewById(R.id.close);
        RecyclerView items = v.findViewById(R.id.items);
        title.setText(ttl);

        AlertDialog ad = new AlertDialog.Builder(ctx).create();
        ad.setView(v);
        ad.show();

        BerkasAdapter berkasAdapter = new BerkasAdapter(this, i -> {
            file_name.setText(i.getJenis());
            ad.dismiss();
        });

        RecyclerView.LayoutManager lm = new LinearLayoutManager(this);
        items.setLayoutManager(lm);
        items.setItemAnimator(new DefaultItemAnimator());
        items.setAdapter(berkasAdapter);
        berkasAdapter.setData(berkas);

        close.setOnClickListener(view -> ad.dismiss());
    }
}