package codepath.demos.helloworlddemo.ui.pelaksana.sugestion;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;

public class SugestionActivity extends AppCompatActivity {
    private Context ctx = this;
    private SugestionAdapter sugestionAdapter;
    private SugestionData sugestionData = new SugestionData();
    private List<SugestionModel> dataFilter = new ArrayList<>();
    private RecyclerView list_sugestion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pegawai_sugestion);
        setToolbar();
        initComponent();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.menu_sugestion);
        toolbar.setBackgroundColor(getResources().getColor(R.color.orange_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.orange_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    private void initComponent() {
        list_sugestion = findViewById(R.id.list_sugestion);
        dataFilter = sugestionData.getListData();
        sugestionAdapter = new SugestionAdapter(ctx, dataFilter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        list_sugestion.setLayoutManager(mLayoutManager);
        list_sugestion.setItemAnimator(new DefaultItemAnimator());
        list_sugestion.setAdapter(sugestionAdapter);

        sugestionAdapter.setOnItemClickListener(id -> {
            Intent i = new Intent(ctx, SugestionDetailActivity.class);
            i.putExtra("id", id);
            ctx.startActivity(i);
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}