package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;

public class FragmentNotVerified extends Fragment {
    private SugestionNotVerifiedAdapter sugestionAdapter;
    private SugestionData sugestionData = new SugestionData();
    private List<SugestionModel> dataFilter = new ArrayList<>();
    private RecyclerView list_sugestion;
    private AppCompatImageButton filter;

    private Context ctx;

    public FragmentNotVerified() {
    }

    public static FragmentNotVerified newInstance(Context ctx) {
        FragmentNotVerified f = new FragmentNotVerified();
        f.ctx = ctx;
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_servicedesk_sugestion_notverified, container, false);
        list_sugestion = root.findViewById(R.id.list_sugestion);
        filter = root.findViewById(R.id.filter);
        initComponent();
        return root;
    }

    private void initComponent() {
        dataFilter = sugestionData.getListData();
        sugestionAdapter = new SugestionNotVerifiedAdapter(ctx, dataFilter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        list_sugestion.setLayoutManager(mLayoutManager);
        list_sugestion.setItemAnimator(new DefaultItemAnimator());
        list_sugestion.setAdapter(sugestionAdapter);

        sugestionAdapter.setOnItemClickListener(id -> {
            Intent i = new Intent(ctx, SugestionDetailActivity.class);
            i.putExtra("id", id);
            ctx.startActivity(i);
        });

        filter.setOnClickListener(v -> {
            showConfirmDialog();
        });
    }

    private void showConfirmDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        LayoutInflater inflater = getLayoutInflater();
        View builder = inflater.inflate(R.layout.filter_sugestion_layout, null);
        dialog.setView(builder);

        final AlertDialog show = dialog.show();

        RadioGroup status = builder.findViewById(R.id.status);
        status.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton checked = radioGroup.findViewById(i);
            boolean isChecked = checked.isChecked();
            if (isChecked) {
                // Changes the textview's text to "Checked: example radiobutton text"
                Toast.makeText(ctx, checked.getText(), Toast.LENGTH_SHORT).show();
                show.dismiss();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.ctx = context;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {
            }
        }
    }
}
