package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import codepath.demos.helloworlddemo.R;

public class SugestionInfoFragment extends Fragment {

    private Context ctx;

    public SugestionInfoFragment() {
    }

    public static SugestionInfoFragment newInstance(Context ctx) {
        SugestionInfoFragment f = new SugestionInfoFragment();
        f.ctx = ctx;
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_servicedesk_sugestion_info, container, false);
        return rootView;
    }
}
