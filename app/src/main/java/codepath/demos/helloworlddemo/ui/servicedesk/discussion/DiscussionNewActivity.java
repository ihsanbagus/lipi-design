package codepath.demos.helloworlddemo.ui.servicedesk.discussion;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;
import codepath.demos.helloworlddemo.helpers.PopUpHelper;

public class DiscussionNewActivity extends AppCompatActivity {
    private Context ctx = this;
    private DiscussionData discussionData = new DiscussionData();
    private List<KawasanModel> listKawasanModel = new ArrayList<>();
    private List<KawasanModel.Layanan> listKawasanModelLayanan = new ArrayList<>();
    private AppCompatTextView kawasan;
    private AppCompatTextView jenisLayanan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pegawai_discussion_new);
        setToolbar();
        initComponent();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.new_discussion);
        toolbar.setBackgroundColor(getResources().getColor(R.color.green_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.green_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    private void initComponent() {
        kawasan = findViewById(R.id.kawasan);
        jenisLayanan = findViewById(R.id.jenis_layanan);
        listKawasanModel = discussionData.getKawasan();

        kawasan.setOnClickListener(view -> {
            List<AppCompatRadioButton> lr = new ArrayList<>();
            AppCompatRadioButton r;
            for (KawasanModel i : listKawasanModel) {
                r = new AppCompatRadioButton(ctx);
                r.setText(i.getKawasan());
                r.setTag(i.getKawasan_id());
                r.setOnClickListener(v -> {
                    listKawasanModelLayanan.clear();
                    listKawasanModelLayanan.addAll(i.getLayanan());
                    jenisLayanan.setText("");
                    jenisLayanan.setEnabled(true);
                    kawasan.setText(i.getKawasan());
                });
                lr.add(r);
            }
            PopUpHelper.popupRadio(ctx, "Pilih Kawasan", lr);
        });

        jenisLayanan.setOnClickListener(view -> {
            List<AppCompatRadioButton> lr = new ArrayList<>();
            AppCompatRadioButton r;
            for (KawasanModel.Layanan i : listKawasanModelLayanan) {
                r = new AppCompatRadioButton(ctx);
                r.setText(i.getLayanan());
                r.setOnClickListener(v -> jenisLayanan.setText(i.getLayanan()));
                lr.add(r);
            }
            PopUpHelper.popupRadio(ctx, "Pilih Jenis Layanan", lr);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_discussion_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_send:
                Toast.makeText(this, "kirim", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
