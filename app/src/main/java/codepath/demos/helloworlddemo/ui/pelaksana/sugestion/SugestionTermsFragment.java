package codepath.demos.helloworlddemo.ui.pelaksana.sugestion;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;

public class SugestionTermsFragment extends Fragment {

    private Context ctx;
    private SugestionTermsAdapter sugestionTermsAdapter;
    private SugestionData sugestionData = new SugestionData();
    private List<SugestionTermsModel> sugestionTermsModels = new ArrayList<>();
    private RecyclerView list_terms;

    public SugestionTermsFragment() {
    }

    public static SugestionTermsFragment newInstance(Context ctx) {
        SugestionTermsFragment f = new SugestionTermsFragment();
        f.ctx = ctx;
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_pegawai_sugestion_terms, container, false);
        list_terms = root.findViewById(R.id.list_terms);
        initComponent();
        return root;
    }

    private void initComponent() {
        sugestionTermsModels = sugestionData.getListTermsData();
        sugestionTermsAdapter = new SugestionTermsAdapter(ctx, sugestionTermsModels);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        list_terms.setLayoutManager(mLayoutManager);
        list_terms.setItemAnimator(new DefaultItemAnimator());
        list_terms.setAdapter(sugestionTermsAdapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.ctx = context;
    }
}
