package codepath.demos.helloworlddemo.ui.manager.discussion;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;

public class DiscussionActivity extends AppCompatActivity {
    private Context ctx = this;
    private DiscussionAdapter discussionAdapter;
    private DiscussionData discussionData = new DiscussionData();
    private List<DiscussionModel> dataFilter = new ArrayList<>();
    private CardView btn_diskusi_baru;
    private RecyclerView list_discussion;
    private AppCompatImageButton filter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_discussion);
        setToolbar();
        initComponent();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.menu_discussion);
        toolbar.setBackgroundColor(getResources().getColor(R.color.blue_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.blue_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    private void initComponent() {
        btn_diskusi_baru = findViewById(R.id.btn_diskusi_baru);
        list_discussion = findViewById(R.id.list_discussion);
        filter = findViewById(R.id.filter);
        dataFilter = discussionData.getListData();
        discussionAdapter = new DiscussionAdapter(ctx, dataFilter);
        btn_diskusi_baru.setOnClickListener(view -> startActivity(new Intent(ctx, DiscussionNewActivity.class)));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        list_discussion.setLayoutManager(mLayoutManager);
        list_discussion.setItemAnimator(new DefaultItemAnimator());
        list_discussion.setAdapter(discussionAdapter);

        discussionAdapter.setOnItemClickListener(id -> {
            Intent i = new Intent(ctx, DiscussionDetailActivity.class);
            i.putExtra("id", id);
            ctx.startActivity(i);
        });

        filter.setOnClickListener(v -> {
            showConfirmDialog();
        });
    }

    private void showConfirmDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View builder = inflater.inflate(R.layout.filter_discussion_layout, null);
        dialog.setView(builder);

        final AlertDialog show = dialog.show();

        RadioGroup status = builder.findViewById(R.id.status);
        status.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton checked = radioGroup.findViewById(i);
            boolean isChecked = checked.isChecked();
            if (isChecked) {
                // Changes the textview's text to "Checked: example radiobutton text"
                Toast.makeText(ctx, checked.getText(), Toast.LENGTH_SHORT).show();
                show.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}