package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;

public class BerkasAdapter extends RecyclerView.Adapter<BerkasAdapter.MyViewHolder> {
    private Context ctx;
    private List<BerkasModel> items = new ArrayList<>();
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(BerkasModel item);
    }

    public BerkasAdapter(Context ctx, OnItemClickListener listener) {
        this.ctx = ctx;
        this.listener = listener;
    }

    public void setData(List<BerkasModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daftar_berkas, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        BerkasModel item = items.get(position);
        holder.jenis.setText(item.getJenis());
        holder.keterangan.setText(item.getKeterangan());
        holder.lihat.setOnClickListener(v -> {
            Intent i = new Intent(ctx, PDFViewerActivity.class);
            i.putExtra("name", item.getJenis());
            i.putExtra("url", item.getUrl());
            ctx.startActivity(i);
        });
        holder.pilih.setOnClickListener(v -> {
            listener.onItemClick(item);
        });
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView jenis;
        AppCompatTextView keterangan;
        AppCompatImageButton lihat;
        AppCompatImageButton pilih;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            jenis = itemView.findViewById(R.id.jenis);
            keterangan = itemView.findViewById(R.id.keterangan);
            lihat = itemView.findViewById(R.id.lihat);
            pilih = itemView.findViewById(R.id.pilih);
        }
    }

}
