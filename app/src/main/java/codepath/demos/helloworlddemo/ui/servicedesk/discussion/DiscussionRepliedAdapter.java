package codepath.demos.helloworlddemo.ui.servicedesk.discussion;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import codepath.demos.helloworlddemo.R;

public class DiscussionRepliedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DiscussionRepliedModel> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public DiscussionRepliedAdapter(Context context, List<DiscussionRepliedModel> items) {
        this.items = items;
        ctx = context;
    }

    public static class OriginalViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView author;
        public AppCompatTextView created;
        public AppCompatTextView body;

        public OriginalViewHolder(View v) {
            super(v);
            author = v.findViewById(R.id.author);
            created = v.findViewById(R.id.created);
            body = v.findViewById(R.id.body);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_pegawai_discussion_replied, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            DiscussionRepliedModel d = items.get(position);
            view.author.setText(d.getAuthor());
            view.created.setText(d.getCreated());
            view.body.setText(d.getBody());
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

}