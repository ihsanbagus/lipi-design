package codepath.demos.helloworlddemo.ui.manager.sugestion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;

public class FragmentRejected extends Fragment {
    private SugestionRejectedAdapter sugestionAdapter;
    private SugestionData sugestionData = new SugestionData();
    private List<SugestionModel> dataFilter = new ArrayList<>();
    private RecyclerView list_sugestion;

    private Context ctx;

    public FragmentRejected() {
    }

    public static FragmentRejected newInstance(Context ctx) {
        FragmentRejected f = new FragmentRejected();
        f.ctx = ctx;
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_manager_sugestion_rejected, container, false);
        list_sugestion = root.findViewById(R.id.list_sugestion);
        initComponent();
        return root;
    }

    private void initComponent() {
        dataFilter = sugestionData.getListData();
        sugestionAdapter = new SugestionRejectedAdapter(ctx, dataFilter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        list_sugestion.setLayoutManager(mLayoutManager);
        list_sugestion.setItemAnimator(new DefaultItemAnimator());
        list_sugestion.setAdapter(sugestionAdapter);

        sugestionAdapter.setOnItemClickListener(id -> {
            Intent i = new Intent(ctx, SugestionDetailActivity.class);
            i.putExtra("id", id);
            ctx.startActivity(i);
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.ctx = context;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {
            }
        }
    }
}
