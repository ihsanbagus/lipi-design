package codepath.demos.helloworlddemo.ui.pelaksana.discussion;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import codepath.demos.helloworlddemo.R;
import codepath.demos.helloworlddemo.utils.Tools;

public class DiscussionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DiscussionModel> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int id);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public DiscussionAdapter(Context context, List<DiscussionModel> items) {
        this.items = items;
        ctx = context;
    }

    public static class OriginalViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView tags;
        public AppCompatImageButton menus;
        public AppCompatTextView title;
        public AppCompatTextView author;
        public AppCompatTextView created;
        public AppCompatTextView status;
        public AppCompatTextView body;
        public AppCompatTextView replied;
        public LinearLayoutCompat reply;
        public LinearLayoutCompat omenu;

        public OriginalViewHolder(View v) {
            super(v);
            tags = v.findViewById(R.id.tags);
            menus = v.findViewById(R.id.menus);
            title = v.findViewById(R.id.title);
            author = v.findViewById(R.id.author);
            created = v.findViewById(R.id.created);
            status = v.findViewById(R.id.status);
            body = v.findViewById(R.id.body);
            replied = v.findViewById(R.id.replied);
            reply = v.findViewById(R.id.reply);
            omenu = v.findViewById(R.id.omenu);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_pegawai_discussion, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            DiscussionModel d = items.get(position);
            view.tags.setText(d.getTag());
            view.status.setBackgroundColor(ctx.getResources().getColor(R.color.orange_400));
            view.reply.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
            view.replied.setOnClickListener(v -> ctx.startActivity(new Intent(ctx, DiscussionDetailActivity.class)));
            view.menus.setOnClickListener(v -> {
                if (view.omenu.getVisibility() == View.GONE) {
                    Tools.slideDown(view.omenu);
                } else {
                    Tools.slideUp(view.omenu);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

}