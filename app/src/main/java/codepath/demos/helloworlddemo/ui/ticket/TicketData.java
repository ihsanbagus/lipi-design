package codepath.demos.helloworlddemo.ui.ticket;

import java.util.ArrayList;
import java.util.List;

public class TicketData {

    public static List<TicketModel> getHistory() {
        TicketModel.History th;
        ArrayList<TicketModel.History> histories = new ArrayList<>();
        TicketModel tm;
        ArrayList<TicketModel> ltm = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                th = new TicketModel.History(
                        j + " Dalam Proses Finalisasi",
                        j + "21 Oktober 2020 10:22",
                        "Sedang Menunggu Proses Dari Pengelola Dokumentasi: Fikri Budiman",
                        "Akbar"
                );
                histories.add(th);
            }
            tm = new TicketModel(
                    i + "AK12345",
                    "Bagus",
                    "123456789",
                    "Pusat Penelitian",
                    "Penerbitan SK Aktif Kembali Tugas Belajar Jabatan Fungsional",
                    histories);
            ltm.add(tm);
        }

        return ltm;
    }
}
