package codepath.demos.helloworlddemo.ui.pelaksana.sugestion;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import codepath.demos.helloworlddemo.R;

public class SugestionStatusFragment extends Fragment {

    private Context ctx;

    public SugestionStatusFragment() {
    }

    public static SugestionStatusFragment newInstance(Context ctx) {
        SugestionStatusFragment f = new SugestionStatusFragment();
        f.ctx = ctx;
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pegawai_sugestion_status, container, false);
        return rootView;
    }
}
