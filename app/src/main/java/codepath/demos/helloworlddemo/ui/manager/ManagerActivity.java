package codepath.demos.helloworlddemo.ui.manager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import codepath.demos.helloworlddemo.R;
import codepath.demos.helloworlddemo.ui.manager.discussion.DiscussionActivity;
import codepath.demos.helloworlddemo.ui.manager.sugestion.SugestionActivity;

public class ManagerActivity extends AppCompatActivity {
    private LinearLayoutCompat sugestion;
    private LinearLayoutCompat discussion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);
        setToolbar();
        initComponent();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.menu_manager);
        toolbar.setBackgroundColor(getResources().getColor(R.color.blue_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.blue_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    private void initComponent() {
        sugestion = findViewById(R.id.sugestion);
        discussion = findViewById(R.id.discussion);

        sugestion.setOnClickListener(view -> startActivity(new Intent(ManagerActivity.this, SugestionActivity.class)));
        discussion.setOnClickListener(view -> startActivity(new Intent(ManagerActivity.this, DiscussionActivity.class)));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}