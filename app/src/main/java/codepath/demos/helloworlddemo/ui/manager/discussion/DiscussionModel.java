package codepath.demos.helloworlddemo.ui.manager.discussion;

public class DiscussionModel {

    private String id;
    private String tag;
    private String title;
    private String author;
    private String created;
    private String body;
    private String countReplied;

    public DiscussionModel() {
    }

    public DiscussionModel(String id, String tag, String title, String author, String created, String body, String countReplied) {
        this.id = id;
        this.tag = tag;
        this.title = title;
        this.author = author;
        this.created = created;
        this.body = body;
        this.countReplied = countReplied;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCountReplied() {
        return countReplied;
    }

    public void setCountReplied(String countReplied) {
        this.countReplied = countReplied;
    }
}
