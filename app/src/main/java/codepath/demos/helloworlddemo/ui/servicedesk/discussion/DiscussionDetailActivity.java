package codepath.demos.helloworlddemo.ui.servicedesk.discussion;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.List;

import codepath.demos.helloworlddemo.R;
import codepath.demos.helloworlddemo.ui.servicedesk.discussion.DiscussionData;

public class DiscussionDetailActivity extends AppCompatActivity {
    private DiscussionData discussionData = new DiscussionData();
    private DiscussionModel dm;
    private DiscussionRepliedAdapter discussionRepliedAdapter;
    private List<DiscussionRepliedModel> discussionRepliedModels;

    private AppCompatTextView tags;
    private AppCompatTextView title;
    private AppCompatTextView author;
    private AppCompatTextView created;
    private AppCompatTextView body;
    private AppCompatTextView replied;
    private LinearLayoutCompat btn_reply;
    private RecyclerView list_replied;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pegawai_discussion_detail);
        setToolbar();
        initComponent();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.detail_discussion);
        toolbar.setBackgroundColor(getResources().getColor(R.color.green_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.green_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    private void initComponent() {
        tags = findViewById(R.id.tags);
        title = findViewById(R.id.title);
        author = findViewById(R.id.author);
        created = findViewById(R.id.created);
        body = findViewById(R.id.body);
        replied = findViewById(R.id.replied);
        btn_reply = findViewById(R.id.btn_reply);
        list_replied = findViewById(R.id.list_replied);

        int id = getIntent().getIntExtra("id", 0);
        dm = discussionData.getDetailData(id);
        tags.setText(dm.getTag());
        title.setText(dm.getTitle());
        author.setText(dm.getAuthor());
        created.setText(dm.getCreated());
        body.setText(dm.getBody());
        replied.setText(dm.getCountReplied());

        btn_reply.setOnClickListener(v -> {
            Intent i = new Intent(this, DiscussionReplyActivity.class);
            i.putExtra("title", dm.getTitle());
            startActivity(i);
        });

        discussionRepliedModels = discussionData.getListReplied();
        discussionRepliedAdapter = new DiscussionRepliedAdapter(this, discussionRepliedModels);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        list_replied.setLayoutManager(mLayoutManager);
        list_replied.setItemAnimator(new DefaultItemAnimator());
        list_replied.setAdapter(discussionRepliedAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
