package codepath.demos.helloworlddemo.ui.ticket;

import java.util.List;

public class TicketModel {
    public String noTiket;
    public String nama;
    public String nip;
    public String statusKerja;
    public String layanan;
    public List<History> histories;

    public String getNoTiket() {
        return noTiket;
    }

    public void setNoTiket(String noTiket) {
        this.noTiket = noTiket;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getStatusKerja() {
        return statusKerja;
    }

    public void setStatusKerja(String statusKerja) {
        this.statusKerja = statusKerja;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public List<History> getHistories() {
        return histories;
    }

    public void setHistories(List<History> histories) {
        this.histories = histories;
    }

    public TicketModel(String noTiket, String nama, String nip, String statusKerja, String layanan, List<History> histories) {
        this.noTiket = noTiket;
        this.nama = nama;
        this.nip = nip;
        this.statusKerja = statusKerja;
        this.layanan = layanan;
        this.histories = histories;
    }

    public static class History {
        public String status;
        public String tanggal;
        public String keterangan;
        public String pemroses;

        public History(String status, String tanggal, String keterangan,String pemroses) {
            this.status = status;
            this.tanggal = tanggal;
            this.keterangan = keterangan;
            this.pemroses = pemroses;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTanggal() {
            return tanggal;
        }

        public void setTanggal(String tanggal) {
            this.tanggal = tanggal;
        }

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }

        public String getPemroses() {
            return pemroses;
        }

        public void setPemroses(String pemroses) {
            this.pemroses = pemroses;
        }
    }
}
