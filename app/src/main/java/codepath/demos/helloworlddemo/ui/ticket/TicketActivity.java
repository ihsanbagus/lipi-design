package codepath.demos.helloworlddemo.ui.ticket;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;
import codepath.demos.helloworlddemo.utils.Tools;

public class TicketActivity extends AppCompatActivity {
    private RecyclerView daftarHistory;
    private AppCompatEditText cari;
    private LinearLayoutCompat layoutHasil;
    private AppCompatImageButton showHide;
    private LinearLayoutCompat layoutShowHide;
    private AppCompatTextView noTicket;
    private AppCompatTextView namaNip;
    private AppCompatTextView statusKerja;
    private AppCompatTextView namaLayanan;

    private List<TicketModel> listTicketModel = new ArrayList<>();
    private TicketData ticketData = new TicketData();
    private TicketAdapter ticketAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_search);
        setToolbar();
        initComponent();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.search_ticket);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.purple_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    private void initComponent() {
        daftarHistory = findViewById(R.id.daftar_history);
        cari = findViewById(R.id.cari);
        layoutHasil = findViewById(R.id.layout_hasil);
        showHide = findViewById(R.id.show_hide);
        layoutShowHide = findViewById(R.id.layout_show_hide);
        noTicket = findViewById(R.id.no_ticket);
        namaNip = findViewById(R.id.nama_nip);
        statusKerja = findViewById(R.id.status_kerja);
        namaLayanan = findViewById(R.id.nama_layanan);

        listTicketModel = ticketData.getHistory();
        ticketAdapter = new TicketAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        daftarHistory.setLayoutManager(mLayoutManager);
        daftarHistory.setItemAnimator(new DefaultItemAnimator());
        daftarHistory.setAdapter(ticketAdapter);

        cari.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pencarian(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        showHide.setOnClickListener(view -> {
            if (layoutShowHide.getVisibility() == View.VISIBLE) {
                layoutShowHide.setVisibility(View.GONE);
                showHide.setImageResource(R.drawable.ic_down);
            } else {
                layoutShowHide.setVisibility(View.VISIBLE);
                showHide.setImageResource(R.drawable.ic_up);
            }
        });
    }

    private void pencarian(String s) {
        for (TicketModel tm : listTicketModel) {
            if (tm.getNoTiket().equalsIgnoreCase(s)) {
                noTicket.setText(tm.getNoTiket());
                namaNip.setText(tm.getNama() + " / " + tm.getNip());
                statusKerja.setText(tm.getStatusKerja());
                namaLayanan.setText(tm.getLayanan());
                ticketAdapter.setData(tm.getHistories());
                layoutHasil.setVisibility(View.VISIBLE);
                break;
            } else {
                layoutHasil.setVisibility(View.GONE);
            }
        }
    }
}