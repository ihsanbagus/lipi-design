package codepath.demos.helloworlddemo.ui.manager.sugestion;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import codepath.demos.helloworlddemo.R;

public class SugestionTrackingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugestion_tracking);
        setToolbar();
    }

    private void setToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.green_700));
        }
    }
}