package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import codepath.demos.helloworlddemo.R;

public class SugestionProofFragment extends Fragment {

    private Context ctx;
    private SugestionProofAdapter sugestionProofAdapter;
    private SugestionData sugestionData = new SugestionData();
    private List<SugestionProofModel> sugestionProofModels = new ArrayList<>();
    private RecyclerView list_proof;

    public SugestionProofFragment() {
    }

    public static SugestionProofFragment newInstance(Context ctx) {
        SugestionProofFragment f = new SugestionProofFragment();
        f.ctx = ctx;
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_servicedesk_sugestion_proof, container, false);
        list_proof = root.findViewById(R.id.list_proof);
        initComponent();
        return root;
    }

    private void initComponent() {
        sugestionProofModels = sugestionData.getListProofData();
        sugestionProofAdapter = new SugestionProofAdapter(ctx, sugestionProofModels);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        list_proof.setLayoutManager(mLayoutManager);
        list_proof.setItemAnimator(new DefaultItemAnimator());
        list_proof.setAdapter(sugestionProofAdapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.ctx = context;
    }
}
