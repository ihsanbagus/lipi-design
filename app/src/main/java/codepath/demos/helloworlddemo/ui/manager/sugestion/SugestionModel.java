package codepath.demos.helloworlddemo.ui.manager.sugestion;

public class SugestionModel {

    private String id;
    private String title;
    private String created;

    public SugestionModel() {
    }

    public SugestionModel(String id, String title, String created) {
        this.id = id;
        this.title = title;
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
