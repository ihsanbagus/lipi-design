package codepath.demos.helloworlddemo.ui.ticket;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import codepath.demos.helloworlddemo.R;

public class TicketAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context ctx;
    private List<TicketModel.History> lth;

    public TicketAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void setData(List<TicketModel.History> lth) {
        this.lth = lth;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_ticket_history, parent, false);
        vh = new TicketAdapter.OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof TicketAdapter.OriginalViewHolder) {
            TicketAdapter.OriginalViewHolder view = (TicketAdapter.OriginalViewHolder) holder;
            TicketModel.History th = lth.get(i);
            view.status.setText(th.getStatus());
            view.tanggal.setText(th.getTanggal());
            view.keterangan.setText(th.getKeterangan());
            view.pemroses.setText(th.getPemroses());
        }
    }

    @Override
    public int getItemCount() {
        return lth == null ? 0 : lth.size();
    }

    static class OriginalViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView status;
        private AppCompatTextView tanggal;
        private AppCompatTextView keterangan;
        private AppCompatTextView pemroses;

        public OriginalViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status);
            tanggal = itemView.findViewById(R.id.tanggal);
            keterangan = itemView.findViewById(R.id.keterangan);
            pemroses = itemView.findViewById(R.id.pemroses);
        }
    }
}
