package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import codepath.demos.helloworlddemo.R;

public class SugestionProofAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SugestionProofModel> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public SugestionProofAdapter(Context context, List<SugestionProofModel> items) {
        this.items = items;
        ctx = context;
    }

    public static class OriginalViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView title_panduan;
        private AppCompatButton file_panduan;
        private AppCompatTextView title_pelaksana;
        private AppCompatButton file_pelaksana;
        private AppCompatTextView title_manager;
        private AppCompatButton file_manager;

        public OriginalViewHolder(View v) {
            super(v);
            title_panduan = v.findViewById(R.id.title_panduan);
            file_panduan = v.findViewById(R.id.file_panduan);
            title_pelaksana = v.findViewById(R.id.title_pelaksana);
            file_pelaksana = v.findViewById(R.id.file_pelaksana);
            title_manager = v.findViewById(R.id.title_manager);
            file_manager = v.findViewById(R.id.file_manager);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_servicedesk_sugestion_proof, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            SugestionProofModel d = items.get(position);
            view.title_panduan.setText(d.getTitle_panduan());
            view.title_pelaksana.setText(d.getTitle_pelaksana());
            view.title_manager.setText(d.getTitle_manager());
            view.file_panduan.setOnClickListener(v -> {
                Toast.makeText(ctx, "panduan", Toast.LENGTH_SHORT).show();
            });
            view.file_pelaksana.setOnClickListener(v -> {
                Toast.makeText(ctx, "pelaksana", Toast.LENGTH_SHORT).show();
            });
            view.file_manager.setOnClickListener(v -> {
                Toast.makeText(ctx, "manager", Toast.LENGTH_SHORT).show();
            });
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

}