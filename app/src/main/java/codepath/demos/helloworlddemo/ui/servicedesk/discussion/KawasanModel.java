package codepath.demos.helloworlddemo.ui.servicedesk.discussion;

import java.util.List;

public class KawasanModel {
    private String kawasan_id;
    private String kawasan;
    private List<Layanan> layanan;

    public KawasanModel(String kawasan_id, String kawasan, List<Layanan> layanan) {
        this.kawasan_id = kawasan_id;
        this.kawasan = kawasan;
        this.layanan = layanan;
    }

    public String getKawasan_id() {
        return kawasan_id;
    }

    public void setKawasan_id(String kawasan_id) {
        this.kawasan_id = kawasan_id;
    }

    public String getKawasan() {
        return kawasan;
    }

    public void setKawasan(String kawasan) {
        this.kawasan = kawasan;
    }

    public List<Layanan> getLayanan() {
        return layanan;
    }

    public void setLayanan(List<Layanan> layanan) {
        this.layanan = layanan;
    }

    static class Layanan {
        private String layanan;

        public Layanan(String layanan) {
            this.layanan = layanan;
        }

        public String getLayanan() {
            return layanan;
        }

        public void setLayanan(String layanan) {
            this.layanan = layanan;
        }
    }
}
