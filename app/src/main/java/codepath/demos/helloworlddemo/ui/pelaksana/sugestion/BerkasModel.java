package codepath.demos.helloworlddemo.ui.pelaksana.sugestion;

public class BerkasModel {
    private String jenis;
    private String keterangan;
    private String url;

    public BerkasModel(String jenis, String keterangan, String url) {
        this.jenis = jenis;
        this.keterangan = keterangan;
        this.url = url;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
