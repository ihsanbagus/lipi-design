package codepath.demos.helloworlddemo.ui.pegawai.discussion;

public class DiscussionRepliedModel {

    private String id;
    private String author;
    private String created;
    private String body;
    private String likes;

    public DiscussionRepliedModel() {
    }

    public DiscussionRepliedModel(String id, String author, String created, String body, String likes) {
        this.id = id;
        this.author = author;
        this.created = created;
        this.body = body;
        this.likes = likes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}
