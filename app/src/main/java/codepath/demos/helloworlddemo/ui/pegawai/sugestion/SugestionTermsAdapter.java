package codepath.demos.helloworlddemo.ui.pegawai.sugestion;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;

import codepath.demos.helloworlddemo.R;

import java.util.List;

public class SugestionTermsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SugestionTermsModel> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public SugestionTermsAdapter(Context context, List<SugestionTermsModel> items) {
        this.items = items;
        ctx = context;
    }

    public static class OriginalViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView title;
        private AppCompatTextView uverif;
        private AppCompatTextView utelaah;
        private AppCompatTextView kverif;
        private AppCompatTextView ktelaah;
        private AppCompatButton template;
        private AppCompatButton berkas;
        private AppCompatButton format;

        public OriginalViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.title);
            uverif = v.findViewById(R.id.uverif);
            utelaah = v.findViewById(R.id.utelaah);
            kverif = v.findViewById(R.id.kverif);
            ktelaah = v.findViewById(R.id.ktelaah);
            template = v.findViewById(R.id.template);
            berkas = v.findViewById(R.id.berkas);
            format = v.findViewById(R.id.format);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_pegawai_sugestion_terms, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            SugestionTermsModel d = items.get(position);
            view.title.setText(d.getTitle());
            view.uverif.setText(d.getUverif());
            view.utelaah.setText(d.getUtelaah());
            view.kverif.setText(d.getKverif());
            view.ktelaah.setText(d.getKtelaah());
            view.template.setOnClickListener(v -> {
                Toast.makeText(ctx, "template", Toast.LENGTH_SHORT).show();
            });
            view.berkas.setOnClickListener(v -> {
                Toast.makeText(ctx, "berkas", Toast.LENGTH_SHORT).show();
            });
            view.format.setOnClickListener(v -> {
                Toast.makeText(ctx, "format", Toast.LENGTH_SHORT).show();
            });
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

}