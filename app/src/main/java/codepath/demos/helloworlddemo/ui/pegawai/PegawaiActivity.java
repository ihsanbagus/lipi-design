package codepath.demos.helloworlddemo.ui.pegawai;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;

import codepath.demos.helloworlddemo.R;
import codepath.demos.helloworlddemo.ui.pegawai.discussion.DiscussionActivity;
import codepath.demos.helloworlddemo.ui.pegawai.sugestion.SugestionActivity;

public class PegawaiActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayoutCompat usulan;
    private LinearLayoutCompat laporan;
    private LinearLayoutCompat diskusi;
    private LinearLayoutCompat distribusi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pegawai);
        setToolbar();
        initcomponent();
    }

    private void initcomponent() {
        usulan = findViewById(R.id.usulan);
        laporan = findViewById(R.id.laporan);
        diskusi = findViewById(R.id.diskusi);
        distribusi = findViewById(R.id.distribusi);
        usulan.setOnClickListener(this);
        laporan.setOnClickListener(this);
        diskusi.setOnClickListener(this);
        distribusi.setOnClickListener(this);
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.menu_pegawai);
        toolbar.setBackgroundColor(getResources().getColor(R.color.red_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.red_700));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.usulan:
                startActivity(new Intent(this, SugestionActivity.class));
                break;
            case R.id.laporan:
                break;
            case R.id.diskusi:
                startActivity(new Intent(this, DiscussionActivity.class));
                break;
            case R.id.distribusi:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}