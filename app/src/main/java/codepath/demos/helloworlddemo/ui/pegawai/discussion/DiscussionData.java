package codepath.demos.helloworlddemo.ui.pegawai.discussion;

import java.util.ArrayList;
import java.util.List;

public class DiscussionData {

    public List<DiscussionModel> getListData() {
        List<DiscussionModel> ld = new ArrayList<>();
        DiscussionModel dm;
        dm = new DiscussionModel(
                "1",
                "Sudah Selesai",
                "#1020 - Pertanyaan Tentang Izin",
                "Bambang",
                "21 Juni 2020 - 13:00",
                "Apakan ini pertanyaan 1 ?",
                "2 balasan"
        );
        ld.add(dm);
        dm = new DiscussionModel(
                "2",
                "Belum Selesai",
                "#1020 - Pertanyaan Tentang Izin",
                "Pras",
                "21 Juni 2020 - 13:00",
                "Apakan ini pertanyaan 2 ?",
                "2 balasan"
        );
        ld.add(dm);
        dm = new DiscussionModel(
                "3",
                "Sudah Selesai",
                "#1020 - Pertanyaan Tentang Izin",
                "Dimas",
                "21 Juni 2020 - 13:00",
                "Apakan ini pertanyaan 3 ?",
                "2 balasan"
        );
        ld.add(dm);
        dm = new DiscussionModel(
                "4",
                "Belum Selesai",
                "#1020 - Pertanyaan Tentang Izin",
                "Hans",
                "21 Juni 2020 - 13:00",
                "Apakan ini pertanyaan 4 ?",
                "2 balasan"
        );
        ld.add(dm);
        return ld;
    }

    public DiscussionModel getDetailData(int id) {
        return getListData().get(id);
    }

    public List<DiscussionRepliedModel> getListReplied() {
        List<DiscussionRepliedModel> ld = new ArrayList<>();
        DiscussionRepliedModel dr;
        dr = new DiscussionRepliedModel(
                "1",
                "Dodo",
                "21 Juli 2020 - 11:00",
                "Izin memotong cuti.",
                "2 suka"
        );
        ld.add(dr);
        dr = new DiscussionRepliedModel(
                "2",
                "Faro",
                "21 Juli 2020 - 11:00",
                "Izin memotong cuti.",
                "2 suka"
        );
        ld.add(dr);
        dr = new DiscussionRepliedModel(
                "3",
                "Jae",
                "21 Juli 2020 - 11:00",
                "Izin memotong cuti.",
                "2 suka"
        );
        ld.add(dr);
        return ld;
    }

    public List<KawasanModel> getKawasan() {
        List<KawasanModel> lkm = new ArrayList<>();
        List<KawasanModel.Layanan> lkml = new ArrayList<>();
        KawasanModel.Layanan kml;
        KawasanModel km = null;
        for (int i = 1; i <= 3; i++) {
            kml = new KawasanModel.Layanan("Layanan A" + i);
            lkml.add(kml);
        }
        km = new KawasanModel("1", "A", lkml);
        lkm.add(km);

        for (int i = 1; i <= 3; i++) {
            kml = new KawasanModel.Layanan("Layanan B" + i);
            lkml.add(kml);
        }
        km = new KawasanModel("2", "B", lkml);
        lkm.add(km);

        return lkm;
    }

}
