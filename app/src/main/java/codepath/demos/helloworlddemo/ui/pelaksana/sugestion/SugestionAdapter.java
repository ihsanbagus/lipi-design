package codepath.demos.helloworlddemo.ui.pelaksana.sugestion;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import codepath.demos.helloworlddemo.R;
import codepath.demos.helloworlddemo.utils.Tools;

public class SugestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SugestionModel> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public SugestionAdapter(Context context, List<SugestionModel> items) {
        this.items = items;
        ctx = context;
    }

    public static class OriginalViewHolder extends RecyclerView.ViewHolder {
        private AppCompatImageButton btn_menu;
        private LinearLayoutCompat omenu;
        private AppCompatTextView title;
        private AppCompatTextView created;
        private LinearLayoutCompat btn_detail;

        public OriginalViewHolder(View v) {
            super(v);
            btn_menu = v.findViewById(R.id.btn_menu);
            omenu = v.findViewById(R.id.omenu);
            title = v.findViewById(R.id.title);
            created = v.findViewById(R.id.created);
            btn_detail = v.findViewById(R.id.btn_detail);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_pegawai_sugestion, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            SugestionModel d = items.get(position);
            view.title.setText(d.getTitle());
            view.created.setText(d.getCreated());
            view.btn_menu.setOnClickListener(v -> {
                if (view.omenu.getVisibility() == View.GONE) {
                    Tools.slideDown(view.omenu);
                } else {
                    Tools.slideUp(view.omenu);
                }
            });
            view.btn_detail.setOnClickListener(v -> {
                Intent i = new Intent(ctx, SugestionDetailActivity.class);
                ctx.startActivity(i);
            });
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

}