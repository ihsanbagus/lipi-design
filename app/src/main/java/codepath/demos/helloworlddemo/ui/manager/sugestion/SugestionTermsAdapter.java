package codepath.demos.helloworlddemo.ui.manager.sugestion;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import codepath.demos.helloworlddemo.R;
import codepath.demos.helloworlddemo.utils.Tools;

public class SugestionTermsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SugestionTermsModel> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public SugestionTermsAdapter(Context context, List<SugestionTermsModel> items) {
        this.items = items;
        ctx = context;
    }

    public static class OriginalViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView title;
        private AppCompatImageButton btn_menu;
        private LinearLayoutCompat omenu;
        private AppCompatImageButton upload;
        private AppCompatTextView uverif;
        private AppCompatTextView utelaah;
        private AppCompatTextView kverif;
        private AppCompatTextView ktelaah;
        private AppCompatButton template;
        private AppCompatButton berkas;
        private AppCompatButton format;

        public OriginalViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.title);
            btn_menu = v.findViewById(R.id.btn_menu);
            omenu = v.findViewById(R.id.omenu);
            upload = v.findViewById(R.id.upload);
            uverif = v.findViewById(R.id.uverif);
            utelaah = v.findViewById(R.id.utelaah);
            kverif = v.findViewById(R.id.kverif);
            ktelaah = v.findViewById(R.id.ktelaah);
            template = v.findViewById(R.id.template);
            berkas = v.findViewById(R.id.berkas);
            format = v.findViewById(R.id.format);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_manager_sugestion_terms, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            SugestionTermsModel d = items.get(position);
            view.title.setText(d.getTitle());
            view.uverif.setText(d.getUverif());
            view.utelaah.setText(d.getUtelaah());
            view.kverif.setText(d.getKverif());
            view.ktelaah.setText(d.getKtelaah());
            view.btn_menu.setOnClickListener(v -> {
                if (view.omenu.getVisibility() == View.GONE) {
                    Tools.slideDown(view.omenu);
                } else {
                    Tools.slideUp(view.omenu);
                }
            });
            view.upload.setOnClickListener(v -> {
                ctx.startActivity(new Intent(ctx, SugestionTermsUploadActivity.class));
            });
            view.template.setOnClickListener(v -> {
                Toast.makeText(ctx, "template", Toast.LENGTH_SHORT).show();
            });
            view.berkas.setOnClickListener(v -> {
                Toast.makeText(ctx, "berkas", Toast.LENGTH_SHORT).show();
            });
            view.format.setOnClickListener(v -> {
                Toast.makeText(ctx, "format", Toast.LENGTH_SHORT).show();
            });
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

}