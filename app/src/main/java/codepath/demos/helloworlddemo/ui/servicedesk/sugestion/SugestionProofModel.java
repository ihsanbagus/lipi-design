package codepath.demos.helloworlddemo.ui.servicedesk.sugestion;

public class SugestionProofModel {

    private String id;
    private String title_panduan;
    private String file_panduan;
    private String title_pelaksana;
    private String file_pelaksana;
    private String title_manager;
    private String file_manager;

    public SugestionProofModel() {
    }

    public SugestionProofModel(String id, String title_panduan, String file_panduan, String title_pelaksana, String file_pelaksana, String title_manager, String file_manager) {
        this.id = id;
        this.title_panduan = title_panduan;
        this.file_panduan = file_panduan;
        this.title_pelaksana = title_pelaksana;
        this.file_pelaksana = file_pelaksana;
        this.title_manager = title_manager;
        this.file_manager = file_manager;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle_panduan() {
        return title_panduan;
    }

    public void setTitle_panduan(String title_panduan) {
        this.title_panduan = title_panduan;
    }

    public String getFile_panduan() {
        return file_panduan;
    }

    public void setFile_panduan(String file_panduan) {
        this.file_panduan = file_panduan;
    }

    public String getTitle_pelaksana() {
        return title_pelaksana;
    }

    public void setTitle_pelaksana(String title_pelaksana) {
        this.title_pelaksana = title_pelaksana;
    }

    public String getFile_pelaksana() {
        return file_pelaksana;
    }

    public void setFile_pelaksana(String file_pelaksana) {
        this.file_pelaksana = file_pelaksana;
    }

    public String getTitle_manager() {
        return title_manager;
    }

    public void setTitle_manager(String title_manager) {
        this.title_manager = title_manager;
    }

    public String getFile_manager() {
        return file_manager;
    }

    public void setFile_manager(String file_manager) {
        this.file_manager = file_manager;
    }
}
