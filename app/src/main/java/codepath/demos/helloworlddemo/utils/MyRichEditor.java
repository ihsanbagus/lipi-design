package codepath.demos.helloworlddemo.utils;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import android.support.v7.widget.AppCompatTextView;

import codepath.demos.helloworlddemo.R;

import jp.wasabeef.richeditor.RichEditor;

public class MyRichEditor {
    public String hasil = "";

    public MyRichEditor(Activity act) {
        RichEditor mEditor;
        AppCompatTextView mPreview;
        mEditor = act.findViewById(R.id.editor);
        mEditor.setEditorHeight(200);
//        mEditor.setEditorFontSize(22);
        mEditor.setEditorFontColor(Color.RED);
//        mEditor.setEditorBackgroundColor(Color.BLUE);
//        mEditor.setBackgroundColor(Color.BLUE);
//        mEditor.setBackgroundResource(R.drawable.bg);
        mEditor.setPadding(10, 10, 10, 10);
//        mEditor.setBackground("https://raw.githubusercontent.com/wasabeef/art/master/chip.jpg");
        mEditor.setPlaceholder("Insert text here...");
        //mEditor.setInputEnabled(false);

        mPreview = act.findViewById(R.id.preview);
        mEditor.setOnTextChangeListener(text -> {
            hasil = text;
            mPreview.setText(text);
        });

        act.findViewById(R.id.action_undo).setOnClickListener(v -> mEditor.undo());

        act.findViewById(R.id.action_redo).setOnClickListener(v -> mEditor.redo());

        act.findViewById(R.id.action_bold).setOnClickListener(v -> mEditor.setBold());

        act.findViewById(R.id.action_italic).setOnClickListener(v -> mEditor.setItalic());

        act.findViewById(R.id.action_subscript).setOnClickListener(v -> mEditor.setSubscript());

        act.findViewById(R.id.action_superscript).setOnClickListener(v -> mEditor.setSuperscript());

        act.findViewById(R.id.action_strikethrough).setOnClickListener(v -> mEditor.setStrikeThrough());

        act.findViewById(R.id.action_underline).setOnClickListener(v -> mEditor.setUnderline());

        act.findViewById(R.id.action_heading1).setOnClickListener(v -> mEditor.setHeading(1));

        act.findViewById(R.id.action_heading2).setOnClickListener(v -> mEditor.setHeading(2));

        act.findViewById(R.id.action_heading3).setOnClickListener(v -> mEditor.setHeading(3));

        act.findViewById(R.id.action_heading4).setOnClickListener(v -> mEditor.setHeading(4));

        act.findViewById(R.id.action_heading5).setOnClickListener(v -> mEditor.setHeading(5));

        act.findViewById(R.id.action_heading6).setOnClickListener(v -> mEditor.setHeading(6));

        act.findViewById(R.id.action_txt_color).setOnClickListener(new View.OnClickListener() {
            private boolean isChanged;
            @Override
            public void onClick(View v) {
                mEditor.setTextColor(isChanged ? Color.BLACK : Color.RED);
                isChanged = !isChanged;
            }
        });

        act.findViewById(R.id.action_bg_color).setOnClickListener(new View.OnClickListener() {
            private boolean isChanged;
            @Override
            public void onClick(View v) {
                mEditor.setTextBackgroundColor(isChanged ? Color.TRANSPARENT : Color.YELLOW);
                isChanged = !isChanged;
            }
        });

        act.findViewById(R.id.action_indent).setOnClickListener(v -> mEditor.setIndent());

        act.findViewById(R.id.action_outdent).setOnClickListener(v -> mEditor.setOutdent());

        act.findViewById(R.id.action_align_left).setOnClickListener(v -> mEditor.setAlignLeft());

        act.findViewById(R.id.action_align_center).setOnClickListener(v -> mEditor.setAlignCenter());

        act.findViewById(R.id.action_align_right).setOnClickListener(v -> mEditor.setAlignRight());

        act.findViewById(R.id.action_blockquote).setOnClickListener(v -> mEditor.setBlockquote());

        act.findViewById(R.id.action_insert_bullets).setOnClickListener(v -> mEditor.setBullets());

        act.findViewById(R.id.action_insert_numbers).setOnClickListener(v -> mEditor.setNumbers());

        act.findViewById(R.id.action_insert_image).setOnClickListener(v -> mEditor.insertImage("https://raw.githubusercontent.com/wasabeef/art/master/chip.jpg", "dachshund", 320));

        act.findViewById(R.id.action_insert_youtube).setOnClickListener(v -> mEditor.insertYoutubeVideo("https://www.youtube.com/embed/pS5peqApgUA"));

        act.findViewById(R.id.action_insert_audio).setOnClickListener(v -> mEditor.insertAudio("https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_5MG.mp3"));

        act.findViewById(R.id.action_insert_video).setOnClickListener(v -> mEditor.insertVideo("https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/1080/Big_Buck_Bunny_1080_10s_10MB.mp4", 360));

        act.findViewById(R.id.action_insert_link).setOnClickListener(v -> mEditor.insertLink("https://github.com/wasabeef", "wasabeef"));

        act.findViewById(R.id.action_insert_checkbox).setOnClickListener(v -> mEditor.insertTodo());
    }

    public String getHasil() {
        return hasil;
    }
}