package codepath.demos.helloworlddemo.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class Tools {

    // slide the view from below itself to the current position
    public static void slideUp(View view) {
        view.setVisibility(View.GONE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public static void slideDown(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                0); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public static void rotate(View view) {
        Animation rotate = new RotateAnimation(0, 360);
        rotate.setDuration(500);
        rotate.setInterpolator(new LinearInterpolator());
        view.startAnimation(rotate);
    }

    public static void setTint(Context ctx, ImageView v, int color) {
        v.setColorFilter(ContextCompat.getColor(ctx, color), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public static Drawable getDrawable(Context ctx, int drawable) {
        return ctx.getResources().getDrawable(drawable);
    }

    public static int getColor(Context ctx, int color) {
        return ctx.getResources().getColor(color);
    }

}
