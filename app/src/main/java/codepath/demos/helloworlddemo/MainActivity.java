package codepath.demos.helloworlddemo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import codepath.demos.helloworlddemo.ui.manager.ManagerActivity;
import codepath.demos.helloworlddemo.ui.pegawai.PegawaiActivity;
import codepath.demos.helloworlddemo.ui.pelaksana.PelaksanaActivity;
import codepath.demos.helloworlddemo.ui.servicedesk.ServiceDeskActivity;
import codepath.demos.helloworlddemo.ui.ticket.TicketActivity;

public class MainActivity extends AppCompatActivity {
    private AppCompatButton pegawai;
    private AppCompatButton servicedesk;
    private AppCompatButton pelaksana;
    private AppCompatButton manager;
    private AppCompatButton ticket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolbar();
        initcomponent();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setBackgroundColor(getResources().getColor(R.color.red_400));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.red_700));
        }
    }

    private void initcomponent() {
        pegawai = findViewById(R.id.pegawai);
        servicedesk = findViewById(R.id.servicedesk);
        pelaksana = findViewById(R.id.pelaksana);
        manager = findViewById(R.id.manager);
        ticket = findViewById(R.id.ticket);
        pegawai.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, PegawaiActivity.class)));
        servicedesk.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, ServiceDeskActivity.class)));
        pelaksana.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, PelaksanaActivity.class)));
        manager.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, ManagerActivity.class)));
        ticket.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, TicketActivity.class)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

}
